package com.wecahr.wecharpay.base;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Properties;

public class properties {

    private static Logger log = LoggerFactory.getLogger(properties.class);

    public static final String WECHAR_INFO = "wechar.properties";


    /** 配置文件中的证书地址. */
    public static final String Filepath = "wechar.filepath";
    /** 配置文件中的appid. */
    public static final String Appid = "wechar.appid";
    /** 配置文件中的商户id. */
    public static final String ID = "wechar.id";
    /** 配置文件中的api密钥 */
    public static final String Key = "wechar.key";


    /** 操作对象. */
    private static properties config = new properties();
    /** 属性文件对象. */
    private Properties properties;



    /**filepath. */
    private String filepath;
    /**appid. */
    private String appid;
    /**id. */
    private String id;
    /** key */
    private String key;

    /**
     * 获取config对象.
     * @return
     */
    public static properties getConfig() {
        return config;
    }


    /**
     * 从classpath路径下加载配置参数
     */
    public void loadPropertiesFromSrc() {
        InputStream in = null;
        try {
            log.info("从classpath: " + properties.class.getClassLoader().getResource("").getPath() + " 获取属性文件" + WECHAR_INFO);
            in = properties.class.getClassLoader().getResourceAsStream(WECHAR_INFO);
            if (null != in) {
                properties = new Properties();
                try {
                    properties.load(in);
                } catch (IOException e) {
                    throw e;
                }
            } else {
                log.error(WECHAR_INFO + "属性文件未能在classpath指定的目录下 " + properties.class.getClassLoader().getResource("").getPath() + " 找到!");
                return;
            }
            loadProperties(properties);
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        } finally {
            if (null != in) {
                try {
                    in.close();
                } catch (IOException e) {
                    log.error(e.getMessage(), e);
                }
            }
        }
    }

    /**
     * 根据传入的  对象设置配置参数
     *
     * @param pro
     */
    public void loadProperties(Properties pro) {
        log.info("开始从属性文件中加载配置项");
        String value = null;

        value = pro.getProperty(Filepath);
        if (!StringUtils.isEmpty(value)) {
            this.filepath = value.trim();
            log.info("配置项：证书的路径==>" + Filepath + "==>" + value + " 已加载");
        }
        value = pro.getProperty(Appid);
        if (!StringUtils.isEmpty(value)) {
            this.appid = value.trim();
            log.info("配置项：Appid==>" + Appid + "==>" + value + " 已加载");
        }
        value = pro.getProperty(ID);
        if (!StringUtils.isEmpty(value)) {
            this.id = value.trim();
            log.info("配置项：商户ID==>" + ID + "==>" + value + " 已加载");
        }
        value = pro.getProperty(Key);
        if (!StringUtils.isEmpty(value)) {
            this.key = value.trim();
            log.info("配置项：api秘钥==>" + Key + "==>" + value + " 已加载");
        }
    }


    /**
     * 从properties文件加载
     *
     * @param rootPath
     *            不包含文件名的目录.
     */
    public void loadPropertiesFromPath(String rootPath) {
        if (StringUtils.isNotBlank(rootPath)) {
            log.info("从路径读取配置文件: " + rootPath + File.separator + WECHAR_INFO);
            File file = new File(rootPath + File.separator + WECHAR_INFO);
            InputStream in = null;
            if (file.exists()) {
                try {
                    in = new FileInputStream(file);
                    properties = new Properties();
                    properties.load(in);
                    loadProperties(properties);
                } catch (FileNotFoundException e) {
                    log.error(e.getMessage(), e);
                } catch (IOException e) {
                    log.error(e.getMessage(), e);
                } finally {
                    if (null != in) {
                        try {
                            in.close();
                        } catch (IOException e) {
                            log.error(e.getMessage(), e);
                        }
                    }
                }
            } else {
                // 由于此时可能还没有完成LOG的加载，因此采用标准输出来打印日志信息
                log.error(rootPath + WECHAR_INFO + "不存在,加载参数失败");
            }
        } else {
            loadPropertiesFromSrc();
        }

    }


    public String getFilepath() {
        return filepath;
    }

    public void setFilepath(String filepath) {
        this.filepath = filepath;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
