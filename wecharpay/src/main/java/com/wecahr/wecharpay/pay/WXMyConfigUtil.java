package com.wecahr.wecharpay.pay;


import com.github.wxpay.sdk.WXPayConfig;
import com.wecahr.wecharpay.base.properties;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

public class WXMyConfigUtil implements WXPayConfig {
    private byte[] certData;


    /**
     * 以下代码暂时没有用到，可能二期会用到
     * @return
     */
    /*public WXMyConfigUtil() throws Exception {

        //加载配置文件
        properties.getConfig().loadPropertiesFromSrc();

        String certPath = properties.getConfig().getFilepath();//从微信商户平台下载的安全证书存放的目录

        File file = new File(certPath);
        InputStream certStream = new FileInputStream(file);
        this.certData = new byte[(int) file.length()];
        certStream.read(this.certData);
        certStream.close();
    }*/

    @Override
    public String getAppID() {
        //加载配置文件
        properties.getConfig().loadPropertiesFromSrc();
        return properties.getConfig().getAppid();
    }

    //parnerid
    @Override
    public String getMchID() {
        //加载配置文件
        properties.getConfig().loadPropertiesFromSrc();
        return properties.getConfig().getId();
    }

    @Override
    public String getKey() {
        //加载配置文件
        properties.getConfig().loadPropertiesFromSrc();
        return properties.getConfig().getKey();
    }

    @Override
    public InputStream getCertStream() {
        ByteArrayInputStream certBis = new ByteArrayInputStream(this.certData);
        return certBis;
    }

    @Override
    public int getHttpConnectTimeoutMs() {
        return 8000;
    }

    @Override
    public int getHttpReadTimeoutMs() {
        return 10000;
    }
}

