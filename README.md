# 微信支付（天使打赏）

#### Description
天使打赏微信支付模块

该模块有两个接口(项目启动端口8888)：
	接口一：下订单
请求地址：/pay	请求方式：get/post
传参：
     * @param out_trade_no      订单号
     * @param total_fee         标价金额
     * @param openid            用户的openid
     * @param spbill_create_ip  发起支付的真实ip地址
     * @param body              商品描述

返回参数：需要前端调用后研究，给前端app返回此字符串，再调用前端的微信sdk引起微信支付

	接口二：手机订单支付完成后回调地址
请求地址：/notify	请求方式：get/post
传参：无

返回参数：异步通知后的XML数据


#### Software Architecture
Software architecture description

#### Installation

1. xxxx
2. xxxx
3. xxxx

#### Instructions

1. xxxx
2. xxxx
3. xxxx

#### Contribution

1. Fork the project
2. Create Feat_xxx branch
3. Commit your code
4. Create Pull Request


#### Gitee Feature

1. You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2. Gitee blog [blog.gitee.com](https://blog.gitee.com)
3. Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4. The most valuable open source project [GVP](https://gitee.com/gvp)
5. The manual of Gitee [http://git.mydoc.io/](http://git.mydoc.io/)
6. The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
